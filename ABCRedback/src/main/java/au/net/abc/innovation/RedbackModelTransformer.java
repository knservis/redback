/**
 * Copyright 2014 Australian Broadcasting Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

package au.net.abc.innovation;

import com.amazonaws.services.kinesis.connectors.KinesisConnectorConfiguration;
import com.amazonaws.services.kinesis.connectors.interfaces.ITransformer;
import com.amazonaws.services.kinesis.model.Record;
import java.io.IOException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import au.net.abc.innovation.kinesis.snowplow.SnowplowEventModel;
import au.net.abc.innovation.kinesis.snowplow.SnowplowEventTransformer;
import au.net.abc.innovation.kinesis.snowplow.SnowplowKinesisHandlerFactory;
import au.net.abc.innovation.kinesis.snowplow.SnowplowKinesisHandlerFactory.IKinesisHandler;

/**
 * This class transforms Snowplow events into a delimited string suitable
 * to be used for the Redshift COPY command on a 0.9.6 Snowplow schema:
 * 
 * see: https://github.com/snowplow/snowplow/blob/0.9.6/4-storage/redshift-storage/sql/atomic-def.sql
 * 
 * @author Sam Mason (sam.mason@abc.net.au)
 */

public class RedbackModelTransformer implements ITransformer<SnowplowEventModel, byte[]> {
    
    private static final Log LOG = LogFactory.getLog(RedbackModelTransformer.class);
    
    private final char delim;
    private final IKinesisHandler handler;
    private final SnowplowEventTransformer transformer;
    
    public RedbackModelTransformer(KinesisConnectorConfiguration config) {
        delim = config.REDSHIFT_DATA_DELIMITER;
        transformer = new SnowplowEventTransformer();
        if(config instanceof RedbackConfiguration){
            RedbackConfiguration redbackConfiguration = (RedbackConfiguration)config;
            handler = SnowplowKinesisHandlerFactory.getHandler(redbackConfiguration.getSnowplowStreamSchemaVersion());
        }
        else{
            handler = SnowplowKinesisHandlerFactory.getHandler(SnowplowKinesisHandlerFactory.CURRENT_VERSION);
        }
    }

    @Override
    public SnowplowEventModel toClass(Record record) throws IOException {
        return createModel(new String(record.getData().array(), "UTF-8"));
    }

    @Override
    public byte[] fromClass(SnowplowEventModel record) throws IOException {
        return toRedshiftDelimitedString(record).getBytes("UTF-8");
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    private SnowplowEventModel createModel(String rawEvent) throws IOException{
        LOG.debug(rawEvent);
        return handler.process(rawEvent);
    }
    
    private String toRedshiftDelimitedString(SnowplowEventModel record) {
        return transformer.toRedshiftDelimitedString(record, delim);
    }
}
