Introduction
--------------------------

ABCRedback reads enriched Snowplow events off a kinesis stream and sends them to an Amazon Redshift instance via S3 using the AWS Kinesis Connector Library (KCL)

The primary motivations are:

	- to simplify the sample KCL code for this specific use case (i.e - endpoints, buckets & cluster already exist, not using dynamo DB etc)
	- to work around the default in-memory model sample that risks out of memory conditions by streaming to S3 from local temporary files rather than RAM
	- to support switching Snowplow event models as the current Kinesis event model does not match the current Redshift event model. Both 0.9.0 and 0.9.6 event models are supported
	- to support configurable options for the COPY into Redshift

Configuration

This is a simplified version of the KCL sample with additional properties for Snowplow and Redshift. 
A sample properties file is included in src/main/resources.

Usage
--------------------------

java -Daws.accessKeyId=<access-key> -Daws.secretKey=<secret-key> -jar target/red-back-1.0.0-jar-with-dependencies.jar -config redback.properties

Change List
--------------------------

	- 22/08/2014 Initial version (Sam.Mason@abc.net.au)
	- 21/10/2014 Updated source to include Apache 2.0 license
