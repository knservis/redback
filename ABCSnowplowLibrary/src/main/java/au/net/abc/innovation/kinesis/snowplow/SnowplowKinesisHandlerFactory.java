/**
 * Copyright 2014 Australian Broadcasting Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */
package au.net.abc.innovation.kinesis.snowplow;

import java.io.IOException;

/** 
 * @author Sam Mason (sam.mason@abc.net.au)
 */

public class SnowplowKinesisHandlerFactory {

    private SnowplowKinesisHandlerFactory(){}
    
    public static final String VERSION_0_9_0        = "0.9.0";
    public static final String VERSION_0_9_6        = "0.9.6";
    public static final String CURRENT_VERSION      = VERSION_0_9_0;
        
    public static interface IKinesisHandler{
        public SnowplowEventModel process(String record) throws IOException;
    }
        
    public static IKinesisHandler getHandler(){
        return getHandler(CURRENT_VERSION);
    }
    
    public static IKinesisHandler getHandler(String version){
        if(version == null){
            throw new IllegalArgumentException("Can't create a handler without a version");
        }
        IKinesisHandler handler = null;
        if(version.equals(VERSION_0_9_0)){
            handler = new SnowplowStreamHandlerv0_9_0();
        }
        else if(version.equals(VERSION_0_9_6)){
            handler = new SnowplowStreamHandlerv0_9_6();
        }
        else{
            throw new IllegalArgumentException("Can't create a handler : unrecognised version " + version);
        }
        return handler;
    }
}
