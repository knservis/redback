/**
 * Copyright 2014 Australian Broadcasting Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */
package au.net.abc.innovation.kinesis.snowplow;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import au.net.abc.innovation.kinesis.snowplow.SnowplowKinesisHandlerFactory.IKinesisHandler;
import java.io.IOException;

/**
 * Base class for handlers - utilities and constants
 * @author Sam Mason (sam.mason@abc.net.au)
 */
public abstract class BaseSnowplowStreamHandler implements IKinesisHandler{
    
    protected static final String TIMESTAMP_FORMAT            = "yyyy-MM-dd HH:mm:ss";
    protected static final String DELIMITER                   = "\t";
    
    @Override
    public abstract SnowplowEventModel process(String record) throws IOException;
    
    protected static Integer parseInteger(String value){
        Integer iValue = null;
        if(value != null){
            try{
                iValue = Integer.valueOf(value);
            }
            catch(NumberFormatException nfe){}
        }
        return iValue;
    }
    
    protected static Short parseShort(String value){
        Short sValue = null;
        if(value != null){
            try{
                sValue = Short.valueOf(value);
            }
            catch(NumberFormatException nfe){}
        }
        return sValue;
    }
    
    protected static Double parseDouble(String value){
        Double dValue = null;
        if(value != null){
            try{
                dValue = Double.valueOf(value);
            }
            catch(NumberFormatException nfe){}
        }
        return dValue;
    }
    
    protected static BigDecimal parseDecimal(String value){
        BigDecimal dValue = null;
        if(value != null){
            try{
                dValue = new BigDecimal(value);
            }
            catch(NumberFormatException nfe){}
        }
        return dValue;
    }
    
    protected static Boolean parseBoolean(String value){
        Boolean bValue = null;
        if(value != null){
            try{
                if(value.equals("1")){
                    bValue = Boolean.TRUE;
                }
                else if(value.equals("0")){
                    bValue = Boolean.FALSE;
                }
                else{
                    bValue = Boolean.parseBoolean(value);
                }
            }
            catch(NumberFormatException nfe){}
        }
        return bValue;
    }
    
    protected static Date parseDate(String value){
        Date dValue = null;
        if(value != null){
            try{
                SimpleDateFormat sdf = new SimpleDateFormat(TIMESTAMP_FORMAT);
                dValue = sdf.parse(value);
            }
            catch(ParseException pe){}
        }
        return dValue;
    }
}
